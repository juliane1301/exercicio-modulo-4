import java.sql.Date;

public class Carro {
    public static String COR = "vermelho";
    public String  nomeDoCarro;
    public Integer quantidadePortas;
    public String numeroChassi;
    public String anoFabricacao;
    public Integer combustivel;
    public Integer quantidadeBanco;
    public Integer quantidadeParafusos;
    public Integer quantidadeCalotas;
    private int quantidadePneus;
    
    public Carro(
         int quantidadePneus,
         Integer quantidadePortas,
         String numeroChassi,
         String anoFabricacao,
         Integer combustivel
    ){
        this.quantidadeParafusos = quantidadePneus * 4;
        this.quantidadeCalotas = quantidadePneus;
        this.quantidadePneus = quantidadePneus + 1;
        this.combustivel = combustivel;
        this.numeroChassi = numeroChassi;
        this.anoFabricacao = anoFabricacao;
    }
   
    public void setCor(String cor){
        this.COR = cor;
     }

    public Integer getQuantidadePneus(){
        return  this.quantidadePneus ;
     }

    public void setQuantidadePorta(Integer quantidadePortas){
        this.quantidadePortas = quantidadePortas;
    }

    public Integer getQuantidadePortas(){
        return  this.quantidadePortas ;
    }
 
    public void setQuantidadeBancos(Integer quantidadeBanco){
        this.quantidadeBanco = quantidadeBanco;
    }

    public Integer getQuantidadeBancos(){
        return  this.quantidadeBanco ;
     }
 
    public void setNomeDoCarro(String nome) {
        this.nomeDoCarro = nome;
    }

    public String getNomeDoCarro() {
        return this.nomeDoCarro;
    }

    public Integer getQuantidaDeCalotas(){
        return this.quantidadeCalotas;
    }

    public Integer getQuantidadeParafusos(){
        return this.quantidadeParafusos;
    }

    public Integer getCombustivel(){
        return this.combustivel;
    }

    public String getNumeroChassi(){
        return this.numeroChassi;
    }

    public String getAnoFabricacao(){
        return this.anoFabricacao;
    }



    public void imprimeValores (){
        System.out.println(" ");
        System.out.println("---- Nome do carro :"+ this.getNomeDoCarro()+ "------");
        System.out.println(" ");
        System.out.println("Quantidade Pneus "+ this.getQuantidadePneus());
        System.out.println("Cor :"+ this.COR);
        System.out.println("Quantidade Portas "+ this.getQuantidadePortas());
        System.out.println("Quantidade Bancos "+ this.getQuantidadeBancos());
        System.out.println("Quantidade Parafusos "+ this.getQuantidadeParafusos());
        System.out.println("Quantidade Calota "+ this.getQuantidaDeCalotas());
        System.out.println("Combustivel "+ getCombustivel());
        System.out.println("Número do Chassi "+ this.getNumeroChassi());
        System.out.println("Ano de Fabricacao "+ this.getAnoFabricacao());

   }

  
}





